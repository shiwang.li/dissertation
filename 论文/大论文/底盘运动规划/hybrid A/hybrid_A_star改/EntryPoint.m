ObstList = []; % Obstacle point list

for i = -25:25
    ObstList(end+1,:) = [i,30];
end
for i = -10:10
    ObstList(end+1,:) = [i, 0];
end
for i = -25:-10
    ObstList(end+1,:) = [i, 5];
end
for i = 10:25
    ObstList(end+1,:) = [i, 5];
end
for i = 0:5
    ObstList(end+1,:) = [10, i];
end
for i = 0:5
    ObstList(end+1,:) = [-10, i];
end

%%
for i = -5:5
    ObstList(end+1,:) = [i, 20];
end
%%

ObstLine = []; % Park lot line for collision check
tLine = [-25, 30 , 25, 30]; %start_x start_y end_x end_y
ObstLine(end+1,:) = tLine;
tLine = [-25, 5, -10, 5];
ObstLine(end+1,:) = tLine;
tLine = [-10, 5, -10, 0];
ObstLine(end+1,:) = tLine;
tLine = [-10, 0, 10, 0];
ObstLine(end+1,:) = tLine;
tLine = [10, 0, 10, 5];
ObstLine(end+1,:) = tLine;
tLine = [10, 5, 25, 5];
ObstLine(end+1,:) = tLine;
tLine = [-25, 5, -25, 30];
ObstLine(end+1,:) = tLine;
tLine = [25, 5, 25, 30];
ObstLine(end+1,:) = tLine;
%%
tLine = [-5, 20, 5, 20];
ObstLine(end+1,:) = tLine;
%%
Vehicle.WB = 2;  % [m] 履带中心之间的宽度
Vehicle.W = 2.31; % [m] 履带之间的宽度
Vehicle.LF = 2.16; % [m] 旋转中心到齿尖的距离
Vehicle.LB = 2; % [m] 旋转中心到最末端的距离
%Vehicle.MAX_STEER = 0.6; % [rad] maximum steering angle 最大转角（方向盘）
Vehicle.MIN_CIRCLE = Vehicle.WB/2; % [m] mininum steering circle radius
Vehicle.MAX_VEL = 2; %[m/s] 履带最大线速度

% ObstList and ObstLine
Configure.ObstList = ObstList;
Configure.ObstLine = ObstLine;

% Motion resolution define
Configure.MOTION_RESOLUTION = 0.1; % [m] path interporate resolution，采样距离=0.1*max_vel，这里作者是希望采样时间0.1s内，vec的前进速度为最大，也就是1m/s
Configure.N_STEER = 20.0; % number of steer command
Configure.EXTEND_AREA = 0; % [m] map extend length
Configure.XY_GRID_RESOLUTION = 2.0; % [m]
Configure.YAW_GRID_RESOLUTION = 0.1; % [rad]   =deg2rad(5.7296)
% Grid bound
Configure.MINX = min(ObstList(:,1))-Configure.EXTEND_AREA;
Configure.MAXX = max(ObstList(:,1))+Configure.EXTEND_AREA;
Configure.MINY = min(ObstList(:,2))-Configure.EXTEND_AREA;
Configure.MAXY = max(ObstList(:,2))+Configure.EXTEND_AREA;
Configure.MINYAW = -pi;
Configure.MAXYAW = pi;
% Cost related define
Configure.SB_COST = 0; % switch back penalty cost，（切换前进后退的代价，只有非直接得到RS路径才会有）
Configure.BACK_COST = 1.5; % backward penalty cost
Configure.STEER_VEL_COST = 6; % steer angle change penalty cost（角速度惩罚，只有非直接得到RS路径才会有）
Configure.STEER_ACC_COST = 120; % steer angle change penalty cost（角加速度惩罚，只有非直接得到RS路径才会有）
Configure.H_COST = 10; % Heuristic cost
% 
% Start = [-15, 13, 0];
% End = [0, 13, pi];

Start = [2, 25, 0];
End = [0, 10, -pi];

% 使用完整约束有障碍情况下用A*搜索的最短路径最为hybrid A*的启发值
ObstMap = GridAStar(Configure.ObstList,End,Configure.XY_GRID_RESOLUTION);
Configure.ObstMap = ObstMap;
cla %  从当前坐标区删除包含可见句柄的所有图形对象。
[x,y,th,D,delta,nodes] = HybridAStar(Start,End,Vehicle,Configure);
% GridAStar(ObstList,End,2);
if isempty(x)
    disp("Failed to find path!")
else
    VehicleAnimation(x,y,th,Configure,Vehicle,nodes)
end